# Character Discussion Generator

A simple set of Python scripts to generate discussion posts, currently mainly for *[Worm](https://parahumans.wordpress.com/ "Worm: A Web Serial")*.

## What is a Discussion Post?

In the sense it is used in this project, a discussion post is simply a text document, formatted with Markdown, which has a variety of facts/tidbits about a certain subject meant to stimulate discussion. Currently, this project is configured to only do this for characters from the previously mentioned web serial *[Worm](https://parahumans.wordpress.com/ "Worm: A Web Serial")*, however I'm planning to expand the project to work more generally for any templatable discussion type.

## Instructions

Module `generate_discussion` is used to generate a new discussion based off of the information found in `data.json`. 

It can be run with Python 3.5 or later using `python generate_discussion.py` or, if multiple versions of Python are installed on your system, `python3 generate_discussion.py`.

The templated text document will be generated based on `template.txt` and stored in `data/`. From there, it may be backed up to a Google Drive account, assuming proper configuration of credentials. (Follow instructions in Storage/Drive/drive_api.py to configure this)

To back up to a configured online storage system, run the `save.py` utility. (See `python save.py -h` for more details)
