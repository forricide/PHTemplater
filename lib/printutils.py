class conditional_print():
    def __init__(self, enabled=True):
        self.enabled = enabled
    def __call__(self, *arg):
        if self.enabled:
            print(*arg)
    def force(self, *arg):
        print(*arg)

def printif(b, *arg):
    if b:
        print(*arg)

def printdict(arg):
    string = ''
    for key in arg:
        string += ("'" + str(key) + "': '" + str(arg[key]) + "' | ")
    print(string[:-2])

# TODO - Just going to leave this here for when feature parity is complete, this needs to have a proper logging system.
