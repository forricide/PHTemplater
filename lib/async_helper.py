import asyncio

def run_async(m):
    return asyncio.get_event_loop().run_until_complete(m)