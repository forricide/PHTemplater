_CONFUTILS_STRONG_VERBOSE = False


# In the future, this could also be loaded from a file. However, this will do for now.
DEFAULTS = {
    'init-file': './data/init.json',
    'metadata-file': './data/meta.json',
    'topics-file': './data/topics.txt',
    'all-topics-file': './data/all_topics.txt'
}


def overwrite_with_warn(variable, value, dictionary):
    if variable in dictionary:
        print('confutils.py:\n\t', 'WARNING: Overwriting variable "' +
              variable + '" (old value "' + dictionary[variable] + '") with value "' + value + '"!')
    dictionary[variable] = value


def is_true(variable, args=[], config={}):
    return ('--' + variable in args) or ((variable in config) and (config[variable].lower() == 'true'))


def is_x(variable, value='true', config={}):
    return (variable in config) and (config[variable].lower() == value)


def get_default(variable, default_value, config={}):
    return default_value if variable not in config else config[variable]


def noop(*args, **kwargs):
    pass


def replace_literals(string, context):
    if _CONFUTILS_STRONG_VERBOSE:
        print('Running confutils.replace_literals on str: "' + string + '", context: "' + str(context) + '"')
    temp = string
    for literal_key in context:
        if type(context[literal_key]) is str:
            temp = temp.replace('!(' + literal_key + ')', context[literal_key])
            if '-' not in literal_key:
                temp = temp.replace('!' + literal_key, context[literal_key])
        else:
            temp = temp.replace('!(' + literal_key + ')', str(context[literal_key]))
            if '-' not in literal_key:  # What does this do?
                temp = temp.replace('!' + literal_key, str(context[literal_key]))
    return temp
