import importlib 
import lib.confutils as cf


__FUNCTIONS__PHTFUT__ = {}  # What does FUT stand for? funcutils?


def fstrip(string, safe=False):
    """"
    :param string: Function call as a string in the form of ${"MODULE NAME": "FUNCTION NAME, ARG_1,...ARG_N"}
    :param safe: If 'safe' is set to True (default False), _fstrip will check to make sure the function
                 is in the form specified.
    :return: Function call in the following form: ["MODULE NAME", "FUNCTION NAME, ARG_1,...ARG_N"]
    """
    if safe and not isinstance(string, str):
        raise TypeError('Argument to _fstrip must be a string!')
    temp = string.strip(' ')
    if safe and (temp[0] is not '$' or temp[1] is not '{' or temp[-1] is not '}'):
        raise ValueError('Argument to _fstrip must be string of the form ${...}!')
    return [x.strip(' ')[1:-1] for x in temp[2:-1].strip(' ').split(':', 1)]


def call(function_obj, config, context):
    """
    This calls a custom 'function object'.
    :param function_obj: This is a custom 'function object' in a list. It should be formatted as follows:
                    ["module name", "function name, argument 1, ... argument n"]
                    THIS IS A LIST. WITH THE SECOND ARGUMENT BEING A STRING. WHICH IS KIND OF A LIST.
                    Yeah I've made some serious mistakes here, I'll be honest...
    :param config:
    :param context:
    :return:
    """
    module = function_obj[0]
    if module not in __FUNCTIONS__PHTFUT__:
        try:
            m = importlib.import_module(module)
        except ImportError:
            print("funcutils.py:\n\tWarning: Could not import " + str(module))
            return "[NO SUCH MODULE: " + str(module) + ']'
        __FUNCTIONS__PHTFUT__[module] = m
    else:
        m = __FUNCTIONS__PHTFUT__[module]
    function_obj = [cf.replace_literals(x.strip(), context) for x in function_obj[1].split(',')]
    # Now that we have the module, we need to call the function
    f_string = function_obj[0].strip("' ")  # This is bad, but shouldn't have negative consequences.
    try:
        f = getattr(m, f_string)
    except AttributeError:
        print("funcutils.py:\n\tWarning: Could not call ", f_string, " from ", str(module))
        return "[NO SUCH FUNCTION: " + str(module) + '.' + f_string + ']'
    # print("Calling with " + str(function[1:]))
    return f(config, context, *(function_obj[1:]))