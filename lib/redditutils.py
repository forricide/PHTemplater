"""
Reddit post parsing specifically for use by stats.py
See: https://praw.readthedocs.io/en/latest/

For now, no CLI/GUI, call directly from python3:
from lib import redditutils as r

Then, if you need to download a url, use
r.download_url(url)

Afterwards, to parse and update everything, run
r.do_everything()
"""

import praw
import json
import re, sys, os
import time

__requests__ = 0
# I love RegEx
# Do you love RegEx?
previous_re = re.compile(
    r'(\[\bPrevious\b[^\]\(]*\([^\]\(]*\)[^\]\(]*\]\()([^\)]*reddit\.com\/r\/Parahumans\/comments\/([^\/]*)[^\)]*)(\))')
next_re = re.compile(
    r'(\[\bNext\b[^\]\(]*\([^\]\(]*\)[^\]\(]*\]\()([^\)]*reddit\.com\/r\/Parahumans\/comments\/([^\/]*)[^\)]*)(\))')


def real_path(path):
    return os.path.join(os.path.dirname(os.path.realpath(__file__)), '../', path)


def get_id_path(kind, reddit_id):
    return real_path('output/reddit/' + kind + '_' + reddit_id + '.s.json')


def get_comments_path(reddit_id):
    return get_id_path('comments', reddit_id)


def get_discussion_path(reddit_id):
    return get_id_path('discussion', reddit_id)


def write_pretty_json(path, data):
    with open(path, 'w') as file:
        json.dump(data, file, indent=4, sort_keys=True)


def url_from_id(reddit_id):
    return 'https://reddit.com/r/Parahumans/comments/' + reddit_id


def get_reddit_instance():
    """
    Initializes our Reddit instance.
    :return: Returns a Reddit instance.
    """
    global __requests__

    auth_path = real_path('Notes/prawauth.prvnt')
    with open(auth_path, 'r') as file:
        json_data = json.load(file)
    ri = praw.Reddit(client_id=json_data["client_id"], client_secret=json_data["client_secret"],
                     password=json_data["password"], user_agent=json_data["user_agent"],
                     username=json_data["username"])
    return ri


def get_no(string=None):
    """
    Gets user input, checks for a non-positive answer.
    :param string: Optional prompt.
    :return: False if the user answers yes, otherwise True.
    """
    ans = input(string) if string is not None else input()
    if ans.lower() not in ['y', 'yes']:
        return True
    return False


def get_general(source, destination):
    """
    Gets some generic Reddit information from source and puts it into destination.
    :param source: Either a Reddit Submission or Comment.
    :param destination: Any dictionary, keys/values will be added.
    :return: No return, uses destination as an out param.
    """
    destination['reddit-id'] = source.id
    if source.author is not None:
        destination['author'] = source.author.name
    else:  # Occurs when the author deleted their account.
        destination['author'] = "[Deleted]"
    destination['timestamp'] = str(source.created_utc)
    destination['permalink'] = 'https://reddit.com' + source.permalink
    destination['score'] = source.score


def get_comments(submission):
    """
    Gets a dictionary containing all comments in a submission.
    :param submission: The Reddit API Submission object.
    :return: A dictionary containing comments.
    """

    comments = {'meta': {}}  # Include a metadata thing for easy information.

    # Warning: This uses up 1 request for each 'More Comments' that it replaces.
    # This script does not need to execute quickly, so do this slowly.
    submission.comments.replace_more(limit=30)

    # .list() flattens the tree
    for comment in submission.comments.list():
        c = {}
        try:
            get_general(comment, c)
            c['fulltext'] = comment.body
            c['edited'] = comment.edited
            c['parent'] = comment.parent_id
        except:
            err = 'comment was none' if comment is None else ('comment(' + str(comment) + ')')
            print('WARNING: get_comments threw an exception:', err)

        comments[c['reddit-id']] = c
    comments['meta']['num-comments'] = len(comments) - 1
    return comments


def download_url(url, fout=None, reddit_instance=None):
    """
    Downloads the discussion and comments at `url`.
    Adds basic information for the discussion to output/reddit/all_discussions.json
    Creates two files: comments_[id].s.json and discussion_[id].s.json
    Where [id] is Reddit's submission id (6 alphanumeric characters)
    :param url: URL for the submission.
    :param fout: Mostly for debugging - instead of using Reddit's submission id for [id], uses this param instead.
    :param reddit_instance: For efficient multiple uses, create a Reddit instance outside this method and pass it in.
    :return: Returns the submission's data for easy use.
    """

    if reddit_instance is None:
        reddit_instance = get_reddit_instance()
    s = reddit_instance.submission(url=url)

    # First scrape submission metadata
    discussion_data = {'link-flair-text': s.link_flair_text, 'num-comments': s.num_comments, 'shortlink': s.shortlink,
                       'fulltext': s.selftext, 'title': s.title}
    get_general(s, discussion_data)

    reddit_id = fout if fout is not None else discussion_data['reddit-id']  # discussion ID

    # Great! Now that we have that, check to see if 
    # this one already exists in our collection.
    with open(real_path('output/reddit/all_discussions.json'), 'r') as file:
        all_discussion_data = json.load(file)

    if discussion_data['reddit-id'] in all_discussion_data:
        print('Reddit ID is identical to one that already exists, for:')
        print(discussion_data['title'])
        if get_no('Continue? Data will be re-parsed completely. '):
            print('Okay, skipping after attempting to get the discussion data.')
            if not os.path.isfile(get_discussion_path(reddit_id)):
                print('Discussion did not exist...')
                return None
            with open(get_discussion_path(reddit_id), 'r') as file:
                return json.load(file)

    # We have two files: One for the discussion text + metadata, another
    # for the comment text + metadata
    # Names are comments_%id.json and discussion_%id.json
    discussion_filename = get_discussion_path(reddit_id)
    comments_filename = get_comments_path(reddit_id)

    # Let's put some minimal information in!
    minformation = {'Number of Comments': discussion_data['num-comments'], 'Submission Score': discussion_data['score'],
                    'Reddit ID': discussion_data['reddit-id'], 'Title': discussion_data['title']}
    all_discussion_data[discussion_data['reddit-id']] = minformation
    if os.path.isfile(discussion_filename) or os.path.isfile(comments_filename):
        print('Found a duplicate file. Overwrite?')
        if get_no():
            print('Warning: Leaving artifact in all_discussion_data.')
            return None

    # Claim this slot for us! This can't possibly go wrong.
    write_pretty_json(real_path('output/reddit/all_discussions.json'), all_discussion_data)

    # Wow! We can just go ahead and write out our entire thing now.
    with open(discussion_filename, 'w') as file:
        json.dump(discussion_data, file, indent=4, sort_keys=True)

    # Comments are a bit harder. We still have to parse... well, pretty
    # much everything about them. And it's not going to be fun. Buckle in!
    comments = get_comments(s)
    if comments is not None:
        with open(comments_filename, 'w') as file:
            json.dump(comments, file, indent=4, sort_keys=True)

    print('We have hypothetically parsed your url...')
    return discussion_data


def try_get_ids_from_discussion(discussion):
    d_text = discussion['fulltext']  # Discussion text

    prev_id = re.search(previous_re, d_text)
    next_id = re.search(next_re, d_text)

    return [prev_id, next_id]


def try_get_ids_from_id(reddit_id):
    if not os.path.isfile(get_discussion_path(reddit_id)):
        return None
    with open(get_discussion_path(reddit_id), 'r') as file:
        return try_get_ids_from_discussion(json.load(file))


def parse_update_discussion(data):
    """
    Takes a dictionary, parses it for extra information, and adds it.
    :param data: Dictionary input.
    :return: No return value, uses data as an out param.
    """
    # This is the method to update for any kind of parsing that doesn't require a Reddit API call

    # Gets [prev, next] and converts None to 'None' for JSON output
    # Looking at this now, not sure stringifying None is necessary

    # Get previous/next urls and ids for easier parsing
    # We might need to re-download the full discussion at one point within this method
    # But for now, it's not really necessary
    ids = [r.group(3) if r is not None else 'None' for r in try_get_ids_from_discussion(data)]
    data['prev-id'] = ids[0]
    data['next-id'] = ids[1]
    urls = ['None' if r == 'None' else url_from_id(r) for r in ids]
    data['prev-url'] = urls[0]
    data['next-url'] = urls[1]

    # Get information about the discussion itself
    num_mo = re.match(r'.+# *([0-9]+) *:.+', data['title'])
    if num_mo is not None:
        data['discussion-number'] = int(num_mo.group(1))  # 1 discussion isn't numbered, 1 is misnumbered (12)
    name_mo = re.search(r'[^#:]+: +(.*)', data['title'])
    if name_mo is not None:
        data['character-name'] = name_mo.group(1)


def parse_update_discussion_from_id(reddit_id):
    # Our typical file i/o guard
    if not os.path.isfile(get_discussion_path(reddit_id)):
        return None
    with open(get_discussion_path(reddit_id), 'r') as file:
        data = json.load(file)
    parse_update_discussion(data)
    write_pretty_json(get_discussion_path(reddit_id), data)
    return data


def parse_update_discussions():
    with open(real_path('output/Reddit/all_discussions.json'), 'r') as file:
        all_disc = json.load(file)
    for key in all_disc:
        new_data = parse_update_discussion_from_id(key)
        all_disc[key]['Title'] = new_data['title']
    write_pretty_json(real_path('output/Reddit/all_discussions.json'), all_disc)


def get_referenced_keys(keys):
    referenced = []
    for key in keys:
        with open(get_discussion_path(key), 'r') as file:
            data = json.load(file)
        if data['next-id'] != 'None':
            referenced.append(data['next-id'])
        if data['prev-id'] != 'None':
            referenced.append(data['prev-id'])
    return referenced


def get_existing_keys():
    with open(real_path('output/Reddit/all_discussions.json'), 'r') as file:
        all_disc = json.load(file)
    return [key for key in all_disc]


def check_list():
    keys = get_existing_keys()
    dnums = []
    ndisc = 0
    for key in keys:
        with open(get_discussion_path(key), 'r') as file:
            disc = json.load(file)
        ndisc += 1
        try:
            dnums.append(int(disc['discussion-number']))
        except KeyError:
            ndisc -= 1
            print('Missing discussion number for id', key)
    dnums.sort()
    allnums = [n for n in range(dnums[0], dnums[-1])]
    missingnums = [n for n in allnums if n not in dnums]
    print('Number of discussions found:', ndisc)
    return missingnums


def do_everything(reddit_instance=None):
    """
    This function does everything.
    Normally this would be a bad idea...
    But in this case, this is just a script to do something, so eh, eventually you need a function that does that thing.
    """
    # https://www.reddit.com/r/help/comments/3p43su/what_is_a_reddit_id/cw33nrt/
    if reddit_instance is None:
        reddit_instance = get_reddit_instance()
    should_wait = False
    while True:
        if should_wait:
            print('Waiting a bit for API reasons.')
            time.sleep(30)  # Prevent Reddit from banning our account. :)
        print('Updating your discussions.')
        parse_update_discussions()
        other_keys = [key for key in get_referenced_keys(get_existing_keys()) if key not in get_existing_keys()]
        if len(other_keys) == 0:
            print('All keys are done. Exiting.')
            break
        print('Currently have found', len(other_keys), 'unparsed keys. Continuing on', str(other_keys[0]) + '...')
        download_url(url_from_id(other_keys[0]), reddit_instance=reddit_instance)
        should_wait = True
        parse_update_discussions()
    if should_wait:
        print('Added new discussions; updating index.')
        generate_index()


class CommentVisitor:
    def __init__(self):
        self.keys = get_existing_keys()
        self.comments = []

    def next(self):
        if len(self.keys) == 0 and len(self.comments) == 0:
            return None
        if len(self.comments) == 0:
            self.get_more_comments()
            return self.next()
        return self.comments.pop(0)

    def get_more_comments(self):
        if len(self.keys) == 0:
            raise IndexError
        ck = self.keys.pop(0)
        with open(get_comments_path(ck), 'r') as c_file:
            cjd = json.load(c_file)
        for key in cjd:
            self.comments.append(cjd[key])


def generate_index():
    keys = get_existing_keys()
    output = []
    for key in keys:
        with open(get_discussion_path(key), 'r') as file:
            disc = json.load(file)

        char_name = disc['character-name']

        # Discussion numbering
        if key not in ['6zfy34']:
            num = 'Bonus MAJOR character discussion' if 'discussion-number' not in disc else 'Character discussion #' + \
                                                                                             str(disc[
                                                                                                     'discussion-number'])
        else:
            if key == '6zfy34':
                num = 'Character discussion #13'
            else:
                num = -1

        # Discussion naming - Temporary fix until I have proper unicode stuff
        if key == 'bnrbfi':
            char_name = "The Yangban"
        elif key == '8hg1k5':
            char_name = "Phir Se"
        elif key == 'azja90':
            char_name = "Uber"

        try:
            output.append('[' + num + ': ' + char_name + '](' + disc['permalink'] + ')')
        except KeyError:
            print('Tried to generate index for', key, 'but something did not exist!')

    with open(real_path('output/reddit/index.txt'), 'wb') as file:
        for x in output:
            ax = x.encode('ascii', 'ignore')
            file.write(ax)
            file.write('\n\n'.encode('ascii', 'ignore'))
