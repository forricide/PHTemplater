"""
Module for getting subjects from the list.
"""

import random
import json
import db

SUBJECT_LIST = 'allchars.txt'

def get_subject(number, list=SUBJECT_LIST):
    """Gets a subject from a number."""
    db.pdeb('Starting subject get.')
    with open(SUBJECT_LIST, 'r') as file:
        data = file.readlines()
    data = [line.strip() for line in data]
    db.pdeb('Finished getting subject, returning subject #{0}'.format(number))
    return data[number]

def get_random_subject():
    """Gets a random subject."""
    db.pdeb('Starting random subject get.')
    number_of_subjects = 0
    db.pdeb('Opening data.json')
    with open('data.json', 'r+') as file:
        json_data = json.load(file)
        number_of_subjects = json_data['numChars']
    db.pdeb('Done with data.json')
    subject_number = random.randint(1, number_of_subjects)
    subject_name = get_subject(subject_number)
    full_char_name = '{0} (#{1})'.format(subject_name, subject_number)
    db.pdeb('Ending random subject get, returning \'{0}\''.format(full_char_name))
    return full_char_name
