import argparse
import os
from os import walk
from os.path import isdir, isfile
import sys
import Storage 

def main(config, context, *args):

    # Create a list to store filenames to be saved.
    to_save = []
    # This keeps track of whether we're adding a single file, or the contents of a folder.
    add_folder = False
    # No filename was specified - we have to add a full folder.
    if config.file_name is None:
        # Saving every options.file_type in options.folder_path
        if not isdir(config.folder_path):
            # The folder provided does not exist; we must exit.
            print('No file provided and \"' + options.folder_path + '\" does not exist. Exiting.')
            sys.exit(1)
        for (dirpath, dirnames, filenames) in walk(options.folder_path):
            # We are adding all of the files in that folder to our list.
            add_folder = True
            to_save.extend(filenames)
            break
    elif not isfile(options.file_name):
        # We were provided a filename, but it does not exist.
        print('The file \"' + options.file_name + '\" does not exist. Exiting.')
        sys.exit(1)
    else:
        # We are only saving the file provided.
        to_save = [options.file_name]

    to_save = [x for x in to_save if x.strip().split('.')[-1] == options.file_type.strip().split('.')[-1]]

    if options.debug:
        print('Uploading the following files: ')
        for filename in to_save:
            print(filename)

    # Create our Drive API wrapper.
    if options.debug:
        w = Storage.Wrapper(mode=Storage.APIMode.DEBUG)
    else:
        w = Storage.Wrapper()

    successful_uploads = []
    preexisting_uploads = []
    unsuccessful_uploads = []

    for filename in to_save:
        if w.isfile(filename):
            # Overwriting is not implemented yet, so we'll just skip files that are already in the drive.
            if options.debug:
                print(filename + ' already exists, skipping.')
            preexisting_uploads.append(filename)
            continue
        if add_folder:
            # We upload the full path including the path of the folder.
            full_name = options.folder_path + '/' + filename
            if options.debug:
                print('Uploading ' + full_name)
            w.upload_file(full_name, parent=w.get_folder_id())
        else:
            # We just upload the file.
            if options.debug:
                print('Uploading ' + filename)
            w.upload_file(filename, parent=w.get_folder_id())
        if w.isfile(filename):
            if options.debug:
                print('Upload of ' + filename + ' succeeded.')
            successful_uploads.append(filename)
        else:
            print('Upload of ' + filename + ' was unsuccessful.')
            unsuccessful_uploads.append(filename)
    
    if options.do_delete:
        safe_delete(successful_uploads, preexisting_uploads, add_folder, options)
    
def safe_delete(filenames, preexisting, add_folder, opts):
    if len(filenames):
        print('The following file(s) uploaded will be removed: ')
        for name in filenames:
            print(name)
        ans = input('Is this all right? [y/n] ')
        if ans == 'y' or ans == 'Y':
            for name in filenames:
                if add_folder:
                    full_name = opts.folder_path + '/' + name
                else:
                    full_name = name
                os.remove(full_name)
    if len(preexisting):
        print('The following file(s) that were skipped due to already being in the drive will be removed: ')
        for name in preexisting:
            print(name)
        ans = input('Is this all right? [y/n] ')
        if ans == 'y' or ans == 'Y':
            for name in preexisting:
                if add_folder:
                    full_name = opts.folder_path + '/' + name
                else:
                    full_name = name
                os.remove(full_name)

class save_opts:
    def __init__(self, do_delete=False, file_name=None, file_type='.txt', folder_path='./data/', debug=False):
        self.do_delete = do_delete
        self.file_name = file_name
        self.file_type = file_type
        self.folder_path = folder_path
        self.debug = debug

def parse_cli():
    parser = argparse.ArgumentParser(description='Saves character files.')
    parser.add_argument('--file', dest='file_name', action='store',
                        default=None,
                        help='Specify a file to save (default: save all unsaved files in ./data)')
    parser.add_argument('--del', dest='delete_files', action='store_const',
                        const=True, default=False,
                        help='Automatically delete all files upon a successful save (default: leave them)')
    parser.add_argument('--verbose', dest='debug', action='store_const',
                        const=True, default=False,
                        help='Print additional debug messages.')
    parser.add_argument('--type', dest='file_type', action='store',
                        default='txt',
                        help='Specify a file type to save (default: save .txt files) \nNote: Has no effect with --file.')
    parser.add_argument('--folder', dest='folder_path', action='store',
                        default='./data/',
                        help='Specify a folder to save (default: ./data/) \nNote: Has no effect with --file.')

    parsed_args = parser.parse_args()
    return parsed_args

def expand_save_opts(parsed_args):
    return save_opts(do_delete=parsed_args.delete_files, 
                     file_name=parsed_args.file_name, 
                     file_type=parsed_args.file_type,
                     folder_path=parsed_args.folder_path,
                     debug=parsed_args.debug)

if __name__ == "__main__":
    main(expand_save_opts(parse_cli()))