######
### THIS FILE IS NO LONGER USED, IT APPEARS
######

"""
This script generates a discussion based on the template contained in template.txt,
along with the data in data.json, and the linked-to strawpoll.
Main functionality:
- Full strawpoll generation and past poll parsing
- Generation of a 'generic comment' for added... um... content.
- Fills out the template with strawpoll links, comment, character names, and discussion number.
"""

import json
import asyncio
from shutil import copyfile
import strawpoll
import relationship
import char
import char_desc
import db
import Storage

GENERATOR_VERSION = '0.1.2'

def remove_character(character_name):
    """Removes a character by name. Fixes number_of_characters. No other side effects."""
    db.pdeb('Removing character {0}'.format(character_name))
    with open('allchars.txt', 'r') as file:
        data = file.readlines()
    data = [line.strip() for line in data]
    try:
        data.remove(character_name)
    except ValueError:
        print('Could not remove ' + character_name + ', may need to manually remove.')
    number_of_characters = len(data)
    with open('allchars.txt', 'w') as file:
        file.write('\n'.join(data))
    try:
        with open('data.json', 'r') as file:
            json_data = json.load(file)
    except IOError:
        db.perr('Could not open data.json!')
        exit(1)
        
    json_data['numChars'] = number_of_characters
    with open('data.json', 'w') as file:
        json.dump(json_data, file, indent=4)

def update_json(key, value):
    """Updates JSON [key] to [value] and returns the original key."""
    return_value = ''
    with open('data.json', 'r+') as file:
        json_data = json.load(file)
        return_value = json_data[key]
        if value == '++':
            json_data[key] = json_data[key] + 1
        else:
            json_data[key] = value
        file.seek(0)
        json.dump(json_data, file, indent=4)
    return return_value

async def get_character_from_poll():
    """Gets the character that was voted last week."""
    api = strawpoll.API()
    url = ''
    with open('data.json', 'r+') as file:
        json_data = json.load(file)
        url = json_data['thisPoll']
    poll = await api.get_poll(url)
    top = 0
    name = ''
    for two in poll.results():
        if two[1] >= top:
            top = two[1]
            name = two[0]
    return name

async def next_strawpoll():
    """Creates the next strawpoll. Adds to last discussion, etc."""
    api = strawpoll.API()
    characters = []
    for _i in range(4):
        characters.append(char.get_random_character())
    number = int(update_json('lastDiscussion', '++'))
    #Ordinals code from https://stackoverflow.com/questions/9647202/ordinal-numbers-replacement
    ordinal = lambda n: "%d%s" % (n, "tsnrhtdd"[(n/10%10 != 1)*(n%10 < 4)*n%10::4])
    number = ordinal(number + 2)
    poll = strawpoll.Poll('What should be the {0} Worm character discussion?'.format(number), characters)
    poll_complete = await api.submit_poll(poll)
    with open('data.json', 'r+') as file:
        json_data = json.load(file)
        json_data['lastPoll'] = json_data['thisPoll']
        json_data['thisPoll'] = poll_complete.url
        file.seek(0)
        json.dump(json_data, file, indent=4)

def template(character):
    """Creates the new file and inserts a filled template."""
    # Create the text file.
    print('\nGenerating text file: ')
    new_file = open('data/{0}.txt'.format(character), 'w+')
    new_file.close()

    # Copy template to the new file.
    print('> Copying template.txt to data/{0}.txt'.format(character))
    copyfile('template.txt', 'data/{0}.txt'.format(character))

    # Set the replacement strings.
    character_name = character
    character_description = char_desc.get_character_description(character_name)

    # Format the new file with the replacement strings.
    print('> Formatting data/{0}.txt'.format(character))
    with open('data/{0}.txt'.format(character), 'r') as file:
        data = file.read()

    with open('data.json', 'r+') as file:
        json_data = json.load(file)

    # Replace the target string
    data = data.replace('$NUM', str(json_data['lastDiscussion']))
    data = data.replace('$CHAR', character_name)
    data = data.replace('$UCHAR', character_name.replace(' ', '_'))
    data = data.replace('$DESC', character_description[0])
    data = data.replace('$LWS', json_data['lastPoll'])
    data = data.replace('$TWS', json_data['thisPoll'])
    data = data.replace('$RLS', relationship.get_character_relationship(character_name))
    data = data.replace('$PRCTG', str(get_percentage()))
    data = data.replace('$VER', GENERATOR_VERSION)

    # Write the file out again
    with open('data/{0}.txt'.format(character), 'w') as file:
        file.write(data)
    print('> Generated data/{0}.txt'.format(character))

def get_percentage():
    """Linters are great, are they not?
       In all seriousness, this gets the percentage of characters that have had discussions."""
    with open('data.json', 'r+') as file:
        json_data = json.load(file)
    currchar = json_data['lastDiscussion']
    percentage = round(currchar*100/(currchar+json_data['numChars']), 2)
    return percentage

def main():
    """A main loop that will run the automatic creation protocols."""
    #Get the highest-voted character from the last strawpoll.
    loop = asyncio.get_event_loop()
    name = loop.run_until_complete(get_character_from_poll()).split(' (', 1)[0]
    loop.close()
    #Remove the new character from the list of possible characters. Note that this
    #ordering is essential so that the removed character cannot be a candidate for the next poll.
    remove_character(name)
    #Generate the next strawpoll, including all random characters.
    asyncio.set_event_loop(asyncio.new_event_loop())
    next_loop = asyncio.get_event_loop().run_until_complete(next_strawpoll())
    #Generate the discussion based on that name and their description.
    template(name)

main()
