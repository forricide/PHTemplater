"""
Reddit post parsing specifically for use by stats.py
See: https://praw.readthedocs.io/en/latest/
"""

import praw
import json
import re
import sys
sys.path.append('../')
import db #pdeb('string')

debug = db.get_instance('rpparse.py')
__requests__ = 0
downloaded_urls = []

def init():
    global __reddit__
    global __requests__ 
    with open('../Notes/prawauth.prvnt', 'r+') as file:
        json_data = json.load(file)
    __reddit__ = praw.Reddit(client_id=json_data["client_id"], client_secret=json_data["client_secret"], password=json_data["password"], user_agent=json_data["user_agent"], username=json_data["username"])

def get_regex_from_post(_url, regex, g=-1):
    """Returns a matching string from a given post URL.
    @param g: If unused, just returns the match object - otherwise, returns the gth group.
    """
    global __requests__

    debug.print('Getting data from: ' + _url, 'Note')
    post = praw.models.Submission(__reddit__, url=_url)
    __requests__+=1
    text = post.selftext
    if g < 0:
        return re.search(regex, text)
    else: 
        test_match = re.search(regex, text)
        if test_match is None:
            debug.print('Could not find a match for URL ' + _url)
            return None
        return test_match.groups()[g]

def get_discussions_from_index(_url):
    global __requests__

    debug.print('Retrieving links from: ' + _url, 'Note')
    post = praw.models.Submission(__reddit__, url=_url)
    text = post.selftext
    regex = r'(\[Character discussion #)([0-9]+)([:][ ])([^\]]*)(\]\()([^\)]*)(\))'
    discussions = {}
    for match in re.finditer(regex, text):
        groups = match.groups()
        # print('#' + groups[1] + ': ' + groups[3] + ', ' + groups[5])
        discussions[groups[3]] = {"Number: ": groups[1], "URL: ": groups[5]}
    return discussions

init()
debug.print(str(__reddit__.user.me()), 'User ID')

def test_index():
    discussions = get_discussions_from_index("https://www.reddit.com/r/Parahumans/comments/6i3s7q/weekly_worm_character_discussion_posts/")
    with open("discussions.json", "w+") as file:
        json.dump(discussions, file, indent=4)

main = test_index

if __name__ == "__main__":
    try:
        main()
    except:
        import sys
        print(sys.exc_info()[0])
        import traceback
        print(traceback.format_exc())
    finally:
        input("Press enter to quit.")