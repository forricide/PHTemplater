import re

tomatch = """**WORM SPOILERS BELOW**

**Weekly Character Discussion: Weld**
---

---

**Cast page description:**
>A member of the Boston Wards, Weld is a metal-skinned boy, one of the case 53s (People who tend to have amnesia, a specific mark and monstrous features).^^[[1]](https://parahumans.wordpress.com/cast-spoiler-free/)

**In-depth:**
>Once the forerunner of a new phase in the Protectorate’s plan to acclimatize the public to capes, Weld was to be the ‘face’ of the more monstrous or unusual capes, taking over the Wards, with plans to eventually have him lead a city’s Protectorate team.  Left the Wards following the revelations about Eidolon and Alexandria’s involvement with Cauldron.  Now leads the Irregulars.  Weld has metal flesh, a consequence of his ability to absorb metal and incorporate it into his biology.  This renders him nigh-indestructible and gives him basic shapeshifting ability.^^[[2]](https://parahumans.wordpress.com/cast-spoiler-free/cast/)

---

**Fanart**:

[Jamming out to music](http://vignette2.wikia.nocookie.net/parahumans/images/e/e8/Weld_by_lonsheep-darj81g.png/revision/latest?cb=20161220203620) (*[lonsheep](http://lonsheep.deviantart.com/)*)

[Full-body art](https://vignette3.wikia.nocookie.net/parahumans/images/a/a1/Weld.jpg/revision/latest?cb=20160601191550)  (*[Pabel and Nine](http://pabelandnine.tumblr.com/)*)

---

**Notes:**

* First appearance: [Extermination 8.1](https://parahumans.wordpress.com/2012/03/03/extermination-8-1/)

* First notable appearance: [Sentinel 9.1](https://parahumans.wordpress.com/2012/04/03/cell-9-1/) (Essentially his interlude)

* General affiliations: Starts as a member of the Boston wards, is transferred to become leader of the Brockton Bay wards following the Leviathan attack, and ends up leading the irregulars after the truth comes out about their origins.

* Powerset: A 'Case 53', Weld is a boy made of metal, he is capable of great feats of strength, does not tire, and is essentially invulnerable. His body can absorb metal as well, increasing his mass, and he can shift that mass to change his body.

* [Wikia page](http://worm.wikia.com/wiki/Weld)

---

|Links|
|:--------------------------------------------------------------------------------------------------------------------------------:|
|*[Last week's strawpoll](https://strawpoll.com/8b9ewc6) - [This week's strawpoll](https://strawpoll.com/s2ba253)*|
|*[Index](https://www.reddit.com/r/Parahumans/comments/6i3s7q/weekly_worm_character_discussion_posts/) - [Next (Accord)](https://www.reddit.com/r/Parahumans/comments/6jkqb3/weekly_worm_character_discussion_2_accord/)*|"""

previous_re = re.compile(r'(\[\bPrevious\b[^\]]*\]\()([^\(]*)(\))')
next_re = re.compile(r'(\[\bNext\b[^\]]*\]\()([^\(]*)(\))')

print(re.search(previous_re, tomatch).groups()[1])
print(re.search(next_re, tomatch).groups()[1])