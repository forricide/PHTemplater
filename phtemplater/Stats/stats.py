"""
A module for gathering statistics on previous discussions and polls.

I might have made a minor mistake and switched strawpoll providers halfway through. So this module provides functionality for stripping results from both strawpoll.com and strawpoll.me

Although the main plan is to use the strawpoll.py python wrapper for strawpoll's api, this might be useful: https://github.com/strawpoll/strawpoll/wiki/API

Also, for a good laugh: https://strawpoll.com/api/
"""

import strawpoll
import json
import re
import bs4
import urllib.request
import rpparse as rpp
import sys
sys.path.append('../')
import db #pdeb('string')

debug = db.get_instance('stats.py')
api = strawpoll.API(requests_policy=strawpoll.RequestsPolicy.synchronous)

first_discussion =  'https://www.reddit.com/r/Parahumans/comments/6i8qzp/weekly_worm_character_discussion_1_weld/'
index = 'https://www.reddit.com/r/Parahumans/comments/6i3s7q/weekly_worm_character_discussion_posts/'
parsed_polls = []

class straw:
    """Strawpoll container class"""
    def __init__(self, url = None):
        if url is None:
            debug.print('URL set to none (__init__() in straw)')
        self.url = url
        self.id = None
        self.ext = None
        self.results = {}
        self.populated = False
    def get_id(self):
        if self.url is None:
            debug.print('Returning None (get_id() in straw)')
            return None
        if self.id is not None:
            return self.id
        url_split = self.url.split('.')
        if len(url_split) == 3:
            url_after = url_split[2].split('/')
        elif len(url_split) == 2: # no www.
            url_after = url_split[1].split('/')
        else:
            return None
        if len(url_after) == 2:
                if url_after[0] == 'me':
                    print('Parsing .me strawpoll.')
                    self.ext = 'me' 
                    return url_after[1]
                elif url_after[0] == 'com':
                    print('Parsing .com strawpoll.')
                    self.ext = 'com'
                    return url_after[1]
    def get_ext(self):
        if self.ext is not None:
            return self.ext
        self.get_id()
        if self.ext is None:
            debug.print('No extension can be provided. (get_ext() in straw)')
        return self.ext
    def get_url(self):
        if self.url is None:
            debug.print('Returning None (get_url() in straw)')
            return None
        else: return self.url
    def get_dict(self, force=False):
        if self.populated == True and not force:
            return self.results
        if self.url is None:
            debug.err('self.url is None, cannot get poll dictionary.')
            return None
        if self.get_ext() == 'me':
            # Can use the API
            debug.print('Getting poll from url: ' + self.url)
            poll = api.get_poll(self.url)
            results = {}
            for result in poll.results():
                results[result[0].split(' (')[0]] = result[1]
            self.populated = True
            return results
        if self.get_ext() == 'com':
            # Cannot use the API (must scrape)
            results = {}
            debug.err('Cannot currently parse .com strawpolls. (get_dict() in straw)')

def get_poll_from_discussion(discussion_link):
    """Returns a poll item."""
    return rpp.get_regex_from_post(discussion_link, r'strawpoll.(com|me)')

def get_next_discussion(discussion_link):
    """Returns a link to the discussion linked within the post passed."""
    return rpp.get_regex_from_post(discussion_link, r'(\[\bNext\b[^\]]*\]\()([^\)]*)(\))', 1)

def main():
    disc = first_discussion
    data = json.load(open('posts.json'))
    prev_urls = []
    MAX_DISC = 100
    for jd in data:
        prev_urls.append(data[jd]["url"])
    disc_itr = 0
    new_disc = 0
    while True:
        disc_itr = disc_itr+1
        print(disc)
        if disc in prev_urls:
            for d in data:
                if data[d]["url"] == disc:
                    if "next_url" in data[d] and data[d]["next_url"] is not None:
                        disc = data[d]["next_url"]
                    else:
                        nd = get_next_discussion(data[d]["url"])
                        if nd is not None:
                            data[d]["next_url"] = nd
        else:
            debug.print('Adding new discussion to posts.json.', 'Note')
            data[len(prev_urls) + new_disc] = {"url": disc}
            new_disc = new_disc + 1
            #debug.print('New data: ' + str(data), 'Info')
        disc = get_next_discussion(disc) 
        if disc is None:
            debug.print('Ending discussion iteration.')
            break
        if disc_itr > MAX_DISC:
            break
    #debug.print('Data is now: ' + str(data), 'Info')
    with open('posts.json', 'w') as file:
        json.dump(data, file, indent=4)
    debug.print('PRAW requests made: ' + str(rpp.__requests__), 'Note')

# one = straw()
# two = straw('https://strawpoll.com/x534xy7w')
# three = straw('http://www.strawpoll.me/15056183')

# print(one.get_id())
# print(two.get_id())
# print(three.get_id())

# print(one.get_dict())
# print(two.get_dict())
# print(three.get_dict())

# print(get_next_discussion(first_discussion))

main()