"""
This module is for unifying the printing of debug messages.
"""

PRINT_DEBUG = True

def perr(message):
    """Use this to print vital error messages."""
    print(message)

def pdeb(message):
    """Use this to print unnecessary debug messages."""
    if PRINT_DEBUG:
        print(message)

def get_instance(scriptname):
    if PRINT_DEBUG:
        return __debug(scriptname, pdeb)
    else:
        return nodebug(scriptname, pdeb)

class __debug():
    def __init__(self, scriptname, debfunc):
        self.debfunc = debfunc
        self.scriptname = scriptname
    def print(self, message, severity = None):
        if severity is None:
            self.debfunc('Warning: ' + message + ' [ ' + self.scriptname + ' ]')
        else:
            self.debfunc(severity + ': ' + message + ' [ ' + self.scriptname + ' ]')
    def err(self, message):
        print('ERROR: ' + message + ' [ ' + self.scriptname + ' ]')

class nodebug():
    """Only prints errors."""
    def __init__(self, scriptname, debfunc):
        self.debfunc = debfunc
        self.scriptname = scriptname
    def print(self, *args):
        pass
    def err(self, message):
        print('ERROR: ' + message + ' [ ' + self.scriptname + ' ]')
