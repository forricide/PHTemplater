"""
A module for web scraping.

Currently, functionality is fairly limited.

Dreams for this scraper involve algorithms to find search terms' links/descriptions on a larger number of websites.
"""

import urllib.request
import sys
import re
from bs4 import BeautifulSoup, NavigableString
import confutils as cf

_WEB_SCRAPER_DISABLED = False
if sys.version_info[0] < 3:
    # from urllib2 import urlopen
    print(
        'This module is currently incompatible with Python versions below 3 and has been automatically disabled. ',
        '(pht_web_scraper.py)')
    _WEB_SCRAPER_DISABLED = True

_HTML_TAG_REGEX = re.compile(r'(<([^>]|\\>)+>)+')


def _get_soup(url_string):
    """Returns the html 'soup' from a given URL."""
    page = urllib.request.urlopen(url_string)
    page_soup = BeautifulSoup(page, 'html.parser')
    return page_soup


def _mprint(*args):
    print('pht_web_scraper: \n', *args)


def _clean(utfstring):
    """
    A small function to deal with some weird characters encountered on non-UTF-8 websites.
    :param utfstring: String to be converted to UTF-8 (kind of)
    :return: utfstring, but... more UTF, right?
    """
    string = u"{}".format(utfstring)
    string = string.replace('\xa0', '')
    string = string.replace('�', '\'')
    string = string.replace('’', '\'') \
        .replace('“', '"') \
        .replace('”', '"')
    string = string.strip(' –-')
    string = "".join(c for c in string if c.isprintable())
    return string


def _clean_tags(htmlstring):
    """
    Removes all HTML tags from a string.
    :param htmlstring: String to be purged of HTML tags
    :return: String... but, you know, purged of HTML tags
    """
    return re.sub(_HTML_TAG_REGEX, '', htmlstring.replace('<br>', '\n'))


def _clean_verbose(utfstring):
    string = utfstring
    string = _clean(string)
    return string


def _is_note(string):
    """
    A very simple method to detect strings that do not contain content - just 'notes'.
    :param string: String in.
    :return: True/False whether the string is a 'note'.
    """
    return string != '' and string[0] == '(' and string[-1] == ')'


def _soup_to_content(soup):
    """
    Tries to get the 'content' portion of a soup representing the HTML structure of a webpage.
    :param soup: BS4-generated soup, not a list
    :return: A stripped down list of soups, or the original soup (but in a list) if it couldn't be minimized
             If the return is non-None, the list contents will not be None
    """
    if soup is None:
        return None
    # First find the page content
    soup2 = soup.findAll('div', attrs={'id': 'content'})
    if not soup2:
        return [soup]
    # Try to find a more specific soup
    # Took this beauty from here, kind of:
    # https://stackoverflow.com/questions/952914/making-a-flat-list-out-of-list-of-lists-in-python
    soup3 = [_soup for _soup_list in [s.findAll('div', attrs={'class': 'entry-content'}) for s in soup2] for _soup in
             _soup_list if _soup is not None]
    if soup3:
        # This is probably the best we'll get, at least for now
        return soup3
    # soup3 was None, but there are more options to try
    soup3 = [_soup for _soup_list in [s.findAll('div', attrs={'class': 'messageContent'}) for s in soup2] for _soup in
             _soup_list if _soup is not None]
    if soup3:
        # This is probably the best we'll get, at least for now
        return soup3
    return soup2


def scrape(config, context, url, term, mode="getdescription"):
    if _WEB_SCRAPER_DISABLED:
        return "ERROR: Web scraper disabled."  # This seems pretty useless, honestly...
    _print = _mprint if cf.is_true("verbose", config=config) else cf.noop
    _print("\tsearching url: ", url, "\tfor term:'" + term + "'")

    description = "ERROR: Description not found for " + term
    found_link = 'no link'
    _note = ''
    retval = ''
    souplist = _soup_to_content(_get_soup(url))
    if not souplist:
        _print('\t', description, '\n\tCould not load the webpage contents.')
        return description

    # Main scraping loop
    # this should be refactored out into a loop specifically for finding a description in a soup
    found = False
    for s in souplist:  # Assume s is not none as per _soup_to_content

        # Also don't know how this works, but it's from here
        # https://stackoverflow.com/questions/15138406/how-can-i-replace-or-remove-html-entities-like-nbsp-using-beautifulsoup-4
        # s = s.prettify(formatter=lambda _repl: _repl.replace(u'\xa0', ' '))

        # Search s for the search term
        # Nevermind the prettify above, this works:
        # https://stackoverflow.com/questions/1168660/javascript-regex-for-whitespace-or-nbsp
        # Thank you so much steamer25, keep on steamin' my man
        nstext = s.find_all(text=re.compile(r'[\s\u00A0]*' + re.escape(term) + r'[\s\u00A0]*'))
        if nstext is None:
            continue
        nstext_list = [string for string in nstext if len(str(string)) < 30]
        if len(nstext_list) < 1:
            continue
        nstext = nstext_list[0]
        if mode == 'getdescription':
            # Search for its sibling (other behaviour not yet implemented)
            # If the description is None, go up until it isn't
            nstext_desc = None
            while nstext_desc is None and nstext is not None:
                nstext_desc = nstext.nextSibling  # Go to the right one
                while nstext_desc is not None:
                    temp = _clean_verbose(_clean_tags(str(nstext_desc)))
                    if temp != '' and not _is_note(temp):
                        break
                    if _is_note(temp):
                        _note = ' ' + temp
                    nstext_desc = nstext_desc.nextSibling  # Go to the right until it's None or we find a description

                nstext = nstext.parent  # Go up one

            if nstext_desc is not None:
                found = True
                description = _clean(_clean_tags(str(nstext_desc)))

        elif mode == 'getlink':
            # We found the word, now is it enclosed in a link?
            # There appears to be a distinct lack of documentation on getting the tags around a string
            # So this will be a bit hack-y
            outer_soup = None
            for _nstext in nstext_list:
                if isinstance(_nstext, NavigableString):
                    _nstext = _nstext.parent
                outer_soup = _nstext.find_all('a', href=True)
                if not outer_soup:
                    _nstext = _nstext.parent
                    outer_soup = _nstext.find_all('a', href=True)
                    if not outer_soup:
                        _nstext = _nstext.parent
                        outer_soup = _nstext.find_all('a', href=True)
            if not outer_soup:
                continue  # Didn't find
            found = True
            found_link = outer_soup[0]['href']

    if mode == 'getdescription':
        if not found:
            _print('\t' + description, '\n\tCould not find ' + term)
            retval = description
        else:
            _print('\t' + description, '\n\tFound from search term ' + term)
            retval = '**' + term + _note + ':** ' + description
    elif mode == 'getlink':
        if not found:
            _print('\n\tCould not find link for ' + term)
            retval = url
        else:
            retval = found_link
    return retval


def test(url='https://parahumans.wordpress.com/cast-spoiler-free/', term='Aegis'):
    return scrape(url, term, "getdescription", {"verbose": "true"})


def simple_scrape(page_soup, search_key):
    # CAST_SIMPLE = 'https://parahumans.wordpress.com/cast-spoiler-free/'
    # CAST_COMPLEX = 'https://parahumans.wordpress.com/cast-spoiler-free/cast/'

    """Returns the description for a key found in the BeautifulSoup page given."""
    all_ul = page_soup_ec.find_all('ul')
    all_li = [ul.find_all('li') for ul in all_ul]
    found = 0

    # for li in all_li:
    #     for section in li:
    #         print ((section.strip()).encode(sys.stdout.encoding, errors='replace'))

    for li_element in all_li:
        for section in li_element:
            name = section.find('strong')
            if name is None:
                print('Name was none, trying \'b\'')
                name = section.find('b')
                if name is None:
                    print('Could not parse this name.')
                    continue
            name_updated = str(name.text.strip()).replace('\u2013', '')
            print(str(' : ') + name_updated)
            if str(name_updated) == search_key or str(name_updated).find(search_key) != -1:
                print(search_key)
                found = 1
                description = str(name.next_sibling)
                # The following snippet was replaced by the regex after.
                # desc_fixed = description.replace(' – ', '', 1)
                # if (desc_fixed == description):
                #     print('Trying other starting value.')
                #     desc_fixed = description.replace('– ', '', 1)
                desc_fixed = re.sub('[ ]?–[ ]?', '', description, 1)
                description = desc_fixed.replace('\xa0', ' ').replace('�', '\'')
                return description
    if found == 0:
        print('Could not find key:')
        print(search_key)
        return 'UNDEF_CHAR'

#print(get_character_description('Doctor Q'))
