import html
import pstraw # New strawpoll API wrapper
import os, json, re
import confutils as cf
from default import default_init
import random


class Initializer:
    def __init__(self, config, verbose):
        self._data = None
        self.config = config
        self.print = print if verbose else cf.noop

    def get_or_create_meta_path(self, context):
        """
        This method attempts to get a path to the "metadata" file.
        If it cannot find a path, it will use a default.
        If the file does not exist, it will attempt to create it using an init-file.
        If the init file does not exist, no further attempts will be made.
        :param context:
        :return: Path to metadata file, or None if one cannot be found or created.
        """
        meta_path = cf.get_default(variable='metadata-file',
                                   default_value=cf.DEFAULTS['metadata-file'],
                                   config=self.config)
        if not os.path.isfile(meta_path):
            # Generate from the initializer file instead.
            init_path = cf.get_default(variable='init-file',
                                       default_value=cf.DEFAULTS['init-file'],
                                       config=self.config)
            if not os.path.isfile(init_path):
                # Okay, we give up. We're simply missing necessary configuration.
                # TODO, have some sensible defaults instead of just crashing.
                print('pht_strawpoll_init:\n\tERROR: No meta information to generate a file from.')
                return None
            # We have a file that we will treat as being 'the last discussion'
            return init_path
        return meta_path

    def next_poll(self, context, current_topic):
        characters = []
        with open(cf.get_default("topics-file", cf.DEFAULTS['topics-file'], self.config), 'r') as topics_file:
            remaining_topics = topics_file.readlines()
        remaining_topics = [x.strip('\n ') for x in remaining_topics if x.strip('\n ') != current_topic.strip('\n ')]

        # remaining_topics is the list of all topics remaining
        while len(characters) < min(4, len(remaining_topics)):
            char = random.choice(remaining_topics)
            if char not in characters:
                characters.append(char)

        # Ordinals code from https://stackoverflow.com/questions/9647202/ordinal-numbers-replacement
        ordinal = lambda n: "%d%s" % (n, "tsnrhtdd"[(n / 10 % 10 != 1) * (n % 10 < 4) * n % 10::4])
        number = ordinal(context['DISCUSSION-NUMBER'] + 1)
        return pstraw.post('What should be the {0} Worm character discussion?'.format(number), characters).url()

    def get_topic(self, context):
        # All information is stored in 'context'
        # This function is meant to retrieve information from the previous 'topic'.

        meta_path = self.get_or_create_meta_path(context)
        if not meta_path:
            print("pht_strawpoll_init:\n\tERROR: Initializer.get_topic() called without a meta or init file.")
            return ''  # TODO - Should probably throw here? Need proper exception handling.

        with open(meta_path, 'r') as file:
            self._data = json.load(file)
        # _data is essentially 'all previous discussions'
        # Now we find the most recent discussion to base things off of
        most_recent_discussion = -1
        most_recent_discussion_key = None
        most_recent_discussion_json = None
        for disc_key in self._data:
            if self._data[disc_key]['DISCUSSION-NUMBER'] > most_recent_discussion:
                most_recent_discussion = self._data[disc_key]['DISCUSSION-NUMBER']
                most_recent_discussion_key = disc_key
                most_recent_discussion_json = self._data[disc_key]
        if most_recent_discussion == -1:
            print("pht_strawpoll_init:\n\t",
                  "ERROR: Initializer.get_topic() called without a meta or init file with a discussion inside.")
            return ''  # TODO - Should probably throw here? Need proper exception handling.

        self.print("pht_strawpoll_init: Initializing with the following data key as previous: " +
                   most_recent_discussion_key)

        url = most_recent_discussion_json['GENERATED-POLL']  # This is the poll that was generated last discussion
        poll = pstraw.get(url=url)
        top_choice = self.get_top_choice(poll)
        top_choice = html.unescape(top_choice)

        ### Now that we have most of the information necessary, start to fill out context

        # Topic information - this discussion
        context['TOPIC-FULL'] = top_choice
        context['TOPIC-NUMBER'] = re.sub(r'[^0-9]*\(#|\)', '', top_choice) # This is now broken (no longer used)
        context['TOPIC-CLIPPED'] = top_choice.rstrip(' -()#0123456789\n')  # True beauty (also not fully used)

        # Before we generate the next discussion, go ahead and remove the current topic from our topics file.
        with open(cf.get_default("topics-file", cf.DEFAULTS['topics-file'], self.config), 'r') as topics_file:
            remaining_topics = topics_file.readlines()
        remaining_topics = [x.strip('\n ') for x in remaining_topics]
        if context['TOPIC-CLIPPED'] not in remaining_topics:
            print('Warning:', context['TOPIC-CLIPPED'], ' was not found in remaining_topics; maybe a bug?')
        else:
            remaining_topics.remove(context['TOPIC-CLIPPED'])
        with open(cf.get_default("topics-file", cf.DEFAULTS['topics-file'], self.config), 'w') as topics_file:
            for line in remaining_topics:
                topics_file.write(line + '\n')

        # Topic information - last discussion
        context['PREVIOUS-TOPIC'] = most_recent_discussion_json['TOPIC']

        # Discussion information - this discussion
        context['DISCUSSION-NUMBER'] = most_recent_discussion + 1
        context['GENERATED-POLL'] = self.next_poll(context, context['TOPIC-CLIPPED'])

        # Discussion information - last discussion
        context['PREVIOUS-POLL'] = url

        ### Now fill out this discussion's metadata

        if 'METADATA' not in context:
            context['METADATA'] = {}
        # We are going to output 'meta' information for a file/discussion
        context['METADATA']['TYPE'] = 'file/discussion'
        context['METADATA']['TOPIC'] = context['TOPIC-CLIPPED']
        context['METADATA']['DISCUSSION-NUMBER'] = context['DISCUSSION-NUMBER']
        context['METADATA']['GENERATED-POLL'] = context['GENERATED-POLL']
        return top_choice

    @staticmethod
    def get_top_choice(poll):
        top_value = 0
        top_choice = ''
        for option in poll.results():
            if option[1] >= top_value:
                top_value = option[1]
                top_choice = option[0]
        return top_choice

