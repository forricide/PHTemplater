import json
import os

with open(os.path.join(os.path.dirname(__file__), 'config.json'), 'r') as file:
    j = json.load(file)

try:
    id = j['storage-conf']['cloud-service']['name']
    if id == 'drive':
        from .Drive.drive_api import Wrapper
        from .Drive.drive_api import APIMode
        CloudStorageCapability = True
except KeyError:
    CloudStorageCapability = False
except ModuleNotFoundError:
    CloudStorageCapability = False