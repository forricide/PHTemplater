from apiclient.discovery import build
from apiclient.http import MediaFileUpload
from httplib2 import Http
from oauth2client import file, client, tools
from enum import Enum
import os
from os.path import isfile


class APIMode(Enum):
    NORMAL = 0
    DEBUG = 1


def warn(string):
    print(string)


def err(string):
    print(string)


class Wrapper:
    """Main Google Drive API wrapper.

    :param auth: The path to a .json file containing access authentication for a user to Google Drive's API. \
    If this is missing, use `creds` instead.
    :param creds: The path to an arbitrary credentials file downloaded from https://console.developers.google.com/
    """

    # This is full read-write access to the drive.
    # It appears that this is the minimum required scope for uploading files.
    _APPLICATION_SCOPE = 'https://www.googleapis.com/auth/drive'
    _FOLDER_NAME = 'PHTemplater Storage'

    def __init__(self, auth='drive_credentials.json', creds='drive_credentials.cred', mode=APIMode.NORMAL):
        """Initializes the API using the credentials/keys stored in drive_credentials.json.

        If you don't have this file, download a set of credentials from Google's developer console:
        https://console.developers.google.com/

        You will have to create a project to do this. This link may also be helpful:
        https://developers.google.com/drive/v3/web/about-sdk
        """

        # This small section is mostly just adapted from Google's API documentation Quickstart.
        authorization_storage_path = os.path.join(os.path.dirname(__file__), auth)
        credential_storage = file.Storage(authorization_storage_path)
        credentials = credential_storage.get()
        if not credentials or credentials.invalid:
            credential_storage_path = os.path.join(os.path.dirname(__file__), creds)
            if not os.path.isfile(credential_storage_path):
                raise FileNotFoundError("Could not find a credentials .cred file for Google Drive with the path",
                                        credential_storage_path)
            flow = client.flow_from_clientsecrets(credential_storage_path, self._APPLICATION_SCOPE)
            credentials = tools.run_flow(flow, credential_storage)
        self.drive_connection = build('drive', 'v3', http=credentials.authorize(Http()))
        self._MODE = mode

    def test_query(self):
        # Call the Drive v3 API
        results = self.drive_connection.files().list(
            pageSize=10, fields="nextPageToken, files(id, name)").execute()
        items = results.get('files', [])
        if not items:
            print('No files found.')
        else:
            print('Files:')
            for item in items:
                print('{0} ({1})'.format(item['name'], item['id']))

    def upload_file(self, path, parent=None):
        if path is None:
            return None

        if not isfile(path):
            err(path + ' is not a valid file.')
            return None

        filename = path.split('/')[-1]

        file_metadata = {
            'name': filename
        }

        if parent is not None:
            file_metadata['parents'] = [parent]

        if self._MODE == APIMode.DEBUG:
            print('Attempting to upload file: ' + filename)

        file_media = MediaFileUpload(path, resumable=True)

        uploaded_file = self.drive_connection.files().create(body=file_metadata, media_body=file_media,
                                                             fields='id').execute()

        if self._MODE == APIMode.DEBUG:
            print('ID of uploaded file: ' + uploaded_file.get('id'))

    def get_folder_id(self, max=10, name=None):
        # Refer to https://developers.google.com/drive/v3/web/search-parameters#examples_for_fileslist
        # For further resources on how to tweak this:
        # https://developers.google.com/drive/v3/reference/files/list

        # Search criteria. Format to search for a name X is: "name='X'". This can be adapted to search for anything.
        if name is None:
            query = "name='" + self._FOLDER_NAME + "'"
        else:
            query = "name='" + name + "'"

        # Creates a list of files matching the query.
        found_files = self.drive_connection.files().list(q=query, spaces="drive", orderBy="createdTime",
                                                         pageSize=max).execute()
        file_items = found_files.get('files', [])

        if self._MODE == APIMode.DEBUG and len(file_items):
            print('[DEBUG] Matching files & folders found:')
            for item in file_items:
                print('Name: {0} | ID: {1}'.format(item['name'], item['id']))

        # Collects all the folders out of the files matching the query.
        file_items = [item for item in file_items if item['mimeType'].strip() == 'application/vnd.google-apps.folder']

        if not file_items:
            warn('Could not find the id for a file.')
            return None

        if len(file_items) > 1:
            warn('There are multiple folders by the specified name.')

        return file_items[0]['id']

    def isfile(self, name, max=10):
        next_page = None
        query = "name='" + name.split('/')[-1] + "'"
        while True:
            found_files = self.drive_connection.files().list(q=query, spaces="drive",
                                                             fields='nextPageToken, files(id, name)',
                                                             orderBy="createdTime", pageSize=max,
                                                             pageToken=next_page).execute()
            file_items = found_files.get('files', [])

            if self._MODE == APIMode.DEBUG:
                print('[DEBUG] Matching files & folders found:')
                for item in file_items:
                    print('        Name: {0} | ID: {1}'.format(item['name'], item['id']))

            # Collects all the files out of the files matching the query.
            # file_items = [item for item in file_items if item['mimeType'].strip() !=
            #               'application/vnd.google-apps.folder']

            if not file_items:
                next_page = found_files.get('nextPageToken', None)
                if next_page is None:
                    return False
            else:
                return True
