from google import google
import re


SEARCH_LENIENCY = 5


def find(config, context, topic):
    search_term = str(topic) + ' ' + str(topic) + ' interlude site:parahumans.wordpress.com'
    print('Searching google for the following search term: ' + search_term)
    results = google.search(search_term, 1)
    max_tries = SEARCH_LENIENCY
    for result in results:
        max_tries -= 1
        name = re.search(r'Interlude [^ ]*', result.name)
        if name is not None:
            name = name.group(0)
            return '[' + name + '](' + result.link + ')'
        name = re.search(r'([^ ]*) \(Interlude\)', result.name)
        if name is not None:
            name = 'Interlude ' + name.group(1)
            return '[' + name + '](' + result.link + ')'
        if max_tries == 0:
            return None
