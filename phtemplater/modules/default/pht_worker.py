import confutils as cf
from os import path
import random, string
import json


def mangle_name(name, delimiter='!'):
    """
    A very simple method to add some random characters to the end of a string.
    :param delimiter: Delimiter between the string and the random characters appended.
    :param name: String you want mangled.
    :return: The string in, plus the delimiter, plus 1-10 random ascii letters.
    """
    return name + delimiter + ''.join(random.choice(string.ascii_letters) for _ in range(random.randint(1, 10)))


class Worker:
    def __init__(self, config, verbose):
        self.config = config
        self.released = False
        self.print = print if verbose else cf.noop

    def work(self, output, context):
        """
        This is a simple worker that just outputs to a file
        :param output: What it is we're writing out, generally comes from the generator.
        :param context: We also write out metadata for the current topic, from context['METADATA'].
        :return: None.
        """
        self.print('Default pht_worker running with config:\n\t', str(self.config),
                   '\nAnd with context:\n\t', str(context))

        # Path to the output file - TODO, perhaps remove 'output-dir' and just have the name configurable
        # The above would require some slight refactoring to pht.py, I think
        _path = path.join(cf.replace_literals(self.config['output-dir'], context),
                          cf.replace_literals(self.config['output-name'], context))
        self.print('Writing to file:\n\t"' + _path + '"')

        with open(_path, 'w') as file:
            file.write(output)

        # Path to the metadata file, unlike the output file this is just the filename (full path) and not dir+name
        _path = cf.replace_literals(cf.get_default(
            variable='metadata-file', default_value='./data/meta.json', config=self.config), context)
        self.print('Writing to file\n\t"' + _path + '"')

        if not path.isfile(_path):
            metadata = {}
        else:
            with open(_path, 'r') as file:
                # This only supports the JSON format. TODO - Try/except here and add a config for error handling
                metadata = json.load(file)

        # Note: We store our dictionary with a key equivalent to the topic, but the key is actually irrelevant
        # It's assumed that all operations on the metadata file will just iterate through keys looking for an attribute
        topic_str = context['TOPIC-CLIPPED']
        if topic_str in metadata:
            topic_str_repl = mangle_name(topic_str)  # TODO - Ensure this is unique.
            print('\tWARNING: Topic ' + topic_str +
                  ' already exists in metadata. Inserting metadata as key: ' + topic_str_repl)
            topic_str = topic_str_repl

        metadata[topic_str] = context['METADATA']
        with open(_path, 'w') as file:
            json.dump(metadata, file, indent=4)

        self.print('\n')
