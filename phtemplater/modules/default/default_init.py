import confutils as cf
import os, json
import datetime


class Initializer:
    def __init__(self, config, verbose):
        self.config = config
        self.print = print if verbose else cf.noop

    async def fill_context(self, context):
        # All information is stored in 'context'

        # This module is meant for generating fully generic information
        today = datetime.datetime.now() + datetime.timedelta(days=1)
        cf.overwrite_with_warn("TODAY_WEEKDAY", today.strftime('%A'), context)
        cf.overwrite_with_warn("TODAY_DATE_NUM", today.strftime('%w'), context)
        cf.overwrite_with_warn("TODAY_DATE_NUM_PADDED", today.strftime('%W'), context)
        cf.overwrite_with_warn("TODAY_MONTH", today.strftime('%B'), context)
        cf.overwrite_with_warn("TODAY_MONTH_NUM", today.month, context)
        cf.overwrite_with_warn("TODAY_YEAR", today.year, context)
        
        tomorrow = datetime.datetime.now()
        cf.overwrite_with_warn("TOMORROW_WEEKDAY", tomorrow.strftime('%A'), context)
        cf.overwrite_with_warn("TOMORROW_DATE_NUM", tomorrow.strftime('%w'), context)
        cf.overwrite_with_warn("TOMORROW_DATE_NUM_PADDED", tomorrow.strftime('%W'), context)
        cf.overwrite_with_warn("TOMORROW_MONTH", tomorrow.strftime('%B'), context)
        cf.overwrite_with_warn("TOMORROW_MONTH_NUM", tomorrow.month, context)
        cf.overwrite_with_warn("TOMORROW_YEAR", tomorrow.year, context)