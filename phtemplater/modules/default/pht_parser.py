import re
import confutils as cf


class Parser:
    def __init__(self, config, verbose):
        self._data_path = config["data-dir"]
        self.config = config
        self.print = print if verbose else cf.noop

    async def parse(self, template):
        """ @arg template
            A string containing the template for the generated file.
        """
        # Run regex here to split into a list
        split_template_bad = [x for x in re.split(r'(\$\{([^\}\'\"]*([\'\"]?)[^\3]*?(\3)[^\}\'\"]*)*?\})', template) if
                              x is not '']
        # Now we fix the list to be beautiful
        split_template = []
        skip = 0
        for x in split_template_bad:
            if skip > 0:
                skip = skip - 1
                continue
            if self.is_function(x):
                skip = 3
            split_template.append(x)
        return split_template

    async def parse_from_file(self, template_file):
        with open(template_file, 'r') as template_f:
            template = template_f.read()
        return await self.parse(template)

    @staticmethod
    def is_function(string):
        # I am slightly concerned that there might be a more efficient method of doing all this.
        return string[0:2] == '${' and string[-1] == '}'

    # def parse_function(self, function):
    #     # Function looks like a dict basically
    #     # Each key is a function call and its value(s) are the argument(s)
    #     # Parse into a list of lists (as we don't want or need key functionality
    #     # Note that currently using an escaped quote is undefined behaviour, it is recommended to just surround in opposite quotes
    #     quote = ''
    #     function_list = []
    #     while True:
    #         for c in function:
    #             if c == ' ':
    #                 c = c[1:-1] # or something like this, google it, also fix this to change the function itself not c...
    #             elif c == '"' or c == "'":
    #                 quote = c
    #             else:
    #                 raise ValueError("Unexpected character " + c + " found while parsing function!")
    #         function = function.split(c, 1) # or similar
    #         # make a function to trim leading spaces
    #         function_list[0] = function[0]
    #         function = function[1]
    #         trim_leading_spaces(function)
    #         if function[0] is not ':':
    #             throw "Unexpected character " + function[0] " found while parsing function!"
    #         function = function[1:-1]
    #         trim_leading_spaces(function)
    #         if function[0] == '"' or function[0] == "'":
