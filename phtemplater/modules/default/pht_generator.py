import confutils as cf
import funcutils as functions


class Generator:
    def __init__(self, config, verbose):
        self._data_path = config["data-dir"]
        self.config = config
        self.released = False
        self.print = print if verbose else cf.noop

    async def generate(self, context, template):
        self.print('Default pht_generator running with config:\n\t' + str(self.config))
        self.print('And with context:\n\t' + str(context))
        output = ''
        for p in template:
            if p[0] == '$':
                self.print('Generating text from function: ' + p)
                f = functions.fstrip(p)
                if f[0] == '_literal':
                    text = cf.replace_literals(f[1], context)
                else:
                    # Call f[0] with arguments f[1]
                    # First, create the function & list of arguments
                    text = functions.call(f, self.config, context)
                if text is not None:
                    output += str(text)
                    self.print('---Resulting text: ' + text)
                else:
                    self.print('---No text resulted.')
            else:
                output += p
        self.print('\n')
        return output
