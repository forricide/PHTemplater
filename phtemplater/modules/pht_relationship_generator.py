try:
    import confutils as cf
except ModuleNotFoundError:
    import lib_path as _path_fixer
    _path_fixer.get_path()
    import confutils as cf
import random
import sys
import json

def _c_print(*args, **kwargs):
    print('pht_quote_generator: \n', *args, **kwargs)


def _parse_character_list(character_list):
    return [line.strip('\n ') for line in character_list if line[0] != '#' and line.strip() != '']


def legacy_generator(config, character, all_characters):
    """
    The old generator for 'Generic Comments'... slightly improved.
    I found the original inspiration for this:
    https://www.reddit.com/r/Parahumans/comments/79oczq/weekly_worm_character_discussion_20_burnscar/dp3kimv/
    :param config: Configuration for the relationship generator.
    :param character: Character to get the relationship for.
    :param all_characters: Contents of a file with all character names in it.
    :return: A string with the relationship for 'character' to another character.
    """

    # First, ensure we can get the strings from the data files
    leading_strings_filename = cf.get_default(variable="leading-strings",
                                              default_value="data/leading_strings.data", config=config)
    joining_strings_filename = cf.get_default(variable="joining-strings",
                                              default_value="data/joining_strings.data", config=config)
    rel_storage_filename = cf.get_default(variable="archive", default_value=None, config=config)

    try:
        with open(leading_strings_filename, 'r') as file:
            leading_data = file.read().splitlines()
        with open(joining_strings_filename, 'r') as file:
            joining_data = file.read().splitlines()
    except IOError:
        return "Legacy generator could not open either " + leading_strings_filename + \
               " or " + joining_strings_filename + "!"

    data = _parse_character_list(all_characters)
    leading_data = [line.strip('\n') for line in leading_data if line.strip()[0] != '#' and line.strip() != '']
    joining_data = [line.strip('\n') for line in joining_data if line.strip()[0] != '#' and line.strip() != '']

    if len(data) == 0 or len(leading_data) == 0 or len(joining_data) == 0:
        return "Legacy generator had no data!"

    data = [line for line in data if line[0] != '#' and line.strip() != '']

    other_character = random.choice(data)

    relationship_ = '*"{0} {1}{2} {3}."* - Generic Comments Bot 2.0'.format(random.choice(leading_data), character,
                                                                            random.choice(joining_data),
                                                                            other_character)

    if rel_storage_filename is not None:
        try:
            with open(rel_storage_filename, 'a') as file:
                file.write(character + ': ' + relationship_ + '\n')
        except IOError:
            _c_print("Warning: Could not open archive file " + rel_storage_filename)

    return relationship_


def improved_generator(conf, character, all_characters):
    """
    :param conf: Configuration for the relationship generator.
    :param character: Character to get the relationship for.
    :param all_characters: A list containing all character names.
    :return: A string with the relationship for 'character' to another character.
    """

    # This generator has three formats:
    #   Old - [Text segment 1] character [Text segment 2] other random character
    #   V2  - [Text segment 1] character [Text segment 2]
    #   V3  - [Text segment 1.1] character [Text segment 1.2]
    #   V4  - character [Text segment 1] other character [Text segment 2]

    if "improved-config" not in conf:
        _c_print("Error: improved-config key not found in relationship-config.")
        return None

    config = conf["improved-config"]

    filenames = [config["old"], config["V1"], config["V2"], config["V3"]]
    data = []

    for filename in filenames:
        if filename is not "None":
            try:
                with open(filename, 'r') as file:
                    file_data = json.load(file)
                data.append(file_data)
            except IOError:
                pass  # We're not going to consider this an error

    if len(data) == 0:
        _c_print("Error: Could not load data from the relationship files.")
        return None

    total_p = 0

    # Now we're going to assume we're okay. We need to choose which format to use.
    for format_ in data:
        if format_["format"][0] <= 0:
            continue
        total_p += format_["format"][0]

    chosen = random.random(0, total_p)

    current_p = 0

    the_format = None

    for format_ in data:
        if format_["format"][0] <= 0:
            continue
        current_p += format_["format"][0]
        if current_p >= chosen:
            the_format = format_
            break

    if the_format is None:
        _c_print("Error: Format is None.")
        return None

    # We have our format now.


_RELATIONSHIP_GENERATORS = {
    "legacy": legacy_generator,
    "improved": improved_generator
}


def generate_full(config, context, character):
    """
    Generates a comment describing the relationship between character, and something/someone else.
    :param config: Dictionary containing information used by the generator.
    :param context: Currently unused.
    :param character: Character to get a relationship for.
    :return: A string containing a "comment" on the character.
    """
    _print = _c_print if cf.is_true("verbose", config=config) else cf.noop
    conf = cf.get_default(variable="relationship-config", default_value=config, config=config)
    all_topics_file = cf.get_default(variable="all-topics-file", default_value=cf.DEFAULTS['all-topics-file'],
                                     config=conf)
    mode = cf.get_default(variable="mode", default_value="legacy", config=conf).lower()

    print_output = False  # This is disabled, we're printing the output from the caller.
    if character == '--CLI':
        # Run in CLI mode
        character = input("Please enter the character you would like to generate a relationship for: ")
        print_output = True

    _print('\t' + 'Running with topics file "' + all_topics_file + '" and mode "' + mode + '"',
           '\n\tGenerating relationship for character', character)
    try:
        with open(all_topics_file, 'r') as file:
            data = file.readlines()
    except IOError:
        _print('Could not open file', all_topics_file)
        return 'pht_relationship_generator: IOError [tried to open file ' + all_topics_file + ']'

    if mode in _RELATIONSHIP_GENERATORS:
        output = _RELATIONSHIP_GENERATORS[mode](conf, character, data)
        #if print_output:
        #    print(output)
        return output
    else:
        _print("Could not find generator", mode + ", did you spell it right?")

if __name__ == "__main__":
    character = sys.argv[1]
    print(generate_full({"verbose": "true", "all-topics-file": "data/allsubjects_original.txt"},{},character))
