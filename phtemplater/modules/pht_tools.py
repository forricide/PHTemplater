import confutils as cf
from os.path import isfile
import json


def _c_print(*args, **kwargs):
    print('pht_tools: \n\t', *args, **kwargs)


def _add(arg1, *args):
    if not args:
        return arg1
    return arg1 + _add(*args)


def add(config, context, *args, **kwargs):
    return _add(*args)


def _sub(arg1, *args):
    if not args:
        return arg1
    return arg1 - _add(*args)


def sub(config, context, *args, **kwargs):
    return _sub(*args)


def subtract(config, context, *args, **kwargs):
    return sub(config, context, *args, **kwargs)


def get_percentage(config, context, topic):
    _print = _c_print #if cf.is_true("verbose", config=config) else cf.noop
    # metadata_file = cf.get_default(variable="metadata-file", default_value=cf.DEFAULTS['metadata-file'], config=config)
    topics_file = cf.get_default(variable="all-topics-file", default_value=cf.DEFAULTS['all-topics-file'], config=config)
    if not isfile(topics_file):
        print("ERROR: Could not find the topics file: " + topics_file + "!")
        return "[GET PERCENTAGE ERROR]"

    edited_topics_file = cf.get_default(variable="topics-file", default_value=cf.DEFAULTS['topics-file'], config=config)
    number_of_discussions_remaining = 0
    if isfile(edited_topics_file):
        with open(edited_topics_file, 'r') as edited_file:
            line_info = edited_file.readlines()
        number_of_discussions_remaining = len(line_info)

    # This is going to be very inefficient
    # number_of_discussions = 0
    # if isfile(metadata_file):
    #     with open(metadata_file, 'r') as meta_file:
    #         meta_information = json.load(meta_file)
    #     for key in meta_information:
    #         if cf.is_x(variable="TYPE", value="file/discussion", config=meta_information[key]):
    #             if meta_information[key]['DISCUSSION-NUMBER'] > number_of_discussions:
    #                 number_of_discussions = meta_information[key]['DISCUSSION-NUMBER']
    # number_of_discussions += 1
    i = 0
    with open(topics_file, 'r') as topics_information:
        line_info = topics_information.readlines()
        i = len(line_info)
    _print("Number of discussions remaining in", edited_topics_file + ":", number_of_discussions_remaining, "Number of discussions completed:", context['DISCUSSION-NUMBER'])
    # return str(round((number_of_discussions * 100) / i, 2))
    tot = number_of_discussions_remaining + context['DISCUSSION-NUMBER']
    return str(round(((tot - number_of_discussions_remaining) * 100) / tot, 2))
