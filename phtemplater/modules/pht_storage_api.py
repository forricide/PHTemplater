import argparse
import os
from os import walk
from os.path import isdir, isfile
import sys
import Storage
import json
import confutils as cf


def _c_print(*args, **kwargs):
    # print("pht_storage_api:\n\t", *args, **kwargs)
    print(*args, **kwargs)


def save(config, context, *args):
    # ######## Initialization / configuration
    _print = _c_print if cf.is_true("verbose", config=config) else cf.noop
    _app_print = _c_print if not cf.is_true("no-print", config=config) else cf.noop

    if "cli-modules" in config and \
            "save-config" in config["cli-modules"] \
            and "config-file" in config["cli-modules"]["save-config"]:
        config_filename = config["cli-modules"]["save-config"]["config-file"]
        _print("Running save with config file:", config_filename)
    else:
        # This is not particularly pleasant, but this stuff can be easily reorganized later
        _app_print('ERROR: config.json must contain config["cli-modules"]["save-config"]["config-file"]')
        return
    try:
        with open(config_filename, 'r') as config_file:
            _m_config = json.load(config_file)
    except IOError:
        _app_print('ERROR: Could not open', config_filename)
        return

    # ######## Runtime
    _save_config = cf.get_default(variable="save-conf", default_value=None, config=_m_config)
    if _save_config is None:
        _app_print('ERROR: No "save-conf" in', config_filename)
    upload_dir = cf.get_default(variable="upload-dir", default_value=None, config=_save_config)
    if _save_config is None:
        _app_print('ERROR: No "upload-dir" in', config_filename, ' > "save-conf"')
        return
    no_upload = cf.is_true(variable="no-upload", args=[], config=_save_config)
    ignore_exts = cf.get_default(variable="ignore-exts", default_value=[], config=_save_config)
    # TODO - Only use one of these, or at least print a warning
    only_exts = cf.get_default(variable="only-exts", default_value=[], config=_save_config)

    # Create a list to store filenames to be saved.
    to_save = []
    if not isdir(upload_dir):
        _app_print('ERROR:', upload_dir, 'is not a directory!')
        return
    for (dirpath, dirnames, filenames) in walk(upload_dir):
        # We are adding all of the files in that folder to our list.
        to_save.extend(filenames)
        break

    to_save = [x for x in to_save if x.strip().split('.')[-1] not in ignore_exts]
    if len(only_exts) > 0:
        to_save = [x for x in to_save if x.strip().split('.')[-1] in only_exts]

    _app_print('Uploading the following files:', *to_save)

    # Create our Drive API wrapper.
    if cf.is_true("verbose", config=config):
        w = Storage.Wrapper(mode=Storage.APIMode.DEBUG)
    else:
        w = Storage.Wrapper()

    successful_uploads = []
    preexisting_uploads = []
    unsuccessful_uploads = []

    for filename in to_save:
        if w.isfile(filename):
            # Overwriting is not implemented yet, so we'll just skip files that are already in the drive.
            _print(filename + ' already exists, skipping.')
            preexisting_uploads.append(filename)
            continue
        # We upload the full path including the path of the folder.
        full_name = os.path.join(upload_dir, filename)
        if not no_upload:
            _print('! Uploading ' + full_name)
            w.upload_file(full_name, parent=w.get_folder_id())
        if w.isfile(filename):
            _print('> Upload of ' + filename + ' succeeded.')
            successful_uploads.append(filename)
        else:
            _print('> Upload of ' + filename + ' was unsuccessful.')
            unsuccessful_uploads.append(filename)

    _app_print('\nUploaded the following files:', *successful_uploads)
    _print('The following pre-existing files were skipped:', *preexisting_uploads)
    _app_print('Failed to upload the following files:', *unsuccessful_uploads)

    # safe_delete(successful_uploads, preexisting_uploads, add_folder, options)


def safe_delete(filenames, preexisting, add_folder, opts):
    if len(filenames):
        print('The following file(s) uploaded will be removed: ')
        for name in filenames:
            print(name)
        ans = input('Is this all right? [y/n] ')
        if ans == 'y' or ans == 'Y':
            for name in filenames:
                if add_folder:
                    full_name = opts.folder_path + '/' + name
                else:
                    full_name = name
                os.remove(full_name)
    if len(preexisting):
        print('The following file(s) that were skipped due to already being in the drive will be removed: ')
        for name in preexisting:
            print(name)
        ans = input('Is this all right? [y/n] ')
        if ans == 'y' or ans == 'Y':
            for name in preexisting:
                if add_folder:
                    full_name = opts.folder_path + '/' + name
                else:
                    full_name = name
                os.remove(full_name)


def parse_cli():
    parser = argparse.ArgumentParser(description='Saves character files.')
    parser.add_argument('--file', dest='file_name', action='store',
                        default=None,
                        help='Specify a file to save (default: save all unsaved files in ./data)')
    parser.add_argument('--del', dest='delete_files', action='store_const',
                        const=True, default=False,
                        help='Automatically delete all files upon a successful save (default: leave them)')
    parser.add_argument('--verbose', dest='debug', action='store_const',
                        const=True, default=False,
                        help='Print additional debug messages.')
    parser.add_argument('--type', dest='file_type', action='store',
                        default='txt',
                        help='Specify a file type to save (default: save .txt files) \nNote: Has no effect with --file.')
    parser.add_argument('--folder', dest='folder_path', action='store',
                        default='./data/',
                        help='Specify a folder to save (default: ./data/) \nNote: Has no effect with --file.')

    parsed_args = parser.parse_args()
    return parsed_args