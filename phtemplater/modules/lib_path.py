import os
import sys
sys.path.insert(0, os.path.join(os.path.abspath(os.path.dirname(__file__)), os.path.normpath('../../lib/')))
sys.path.insert(0, os.path.join(os.path.abspath(os.path.dirname(__file__)), os.path.normpath('./default')))
sys.path.insert(0, os.path.join(os.path.abspath(os.path.dirname(__file__)), os.path.normpath('./')))

def get_path():
    return sys.path