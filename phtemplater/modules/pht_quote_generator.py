import confutils as cf


def _c_print(*args, **kwargs):
    print('pht_quote_generator: \n', *args, **kwargs)


def generate(config, context, character):
    _print = _c_print if cf.is_true("verbose", config=config) else cf.noop
    _print('\nGenerating quote for character', character)
    return '>'
