#!/usr/bin/env python3

import phtemplater.modules.lib_path as _path_fixer

_path_fixer.get_path()
import json
from lib.async_helper import run_async
import sys, os
import lib.confutils as cf
import lib.funcutils as futils


def _c_print(*args, **kwargs):
    print(*args, **kwargs)


def main(_arguments):
    # Load the basic configuration
    with open('config.json', 'r') as conf:
        _config = json.load(conf)
    verbose = cf.is_true('verbose', args=_arguments, config=_config)
    _config["verbose"] = "True" if verbose else "False"
    _print = _c_print if verbose else cf.noop
    context = {}

    # Run an alternate workflow if requested
    for arg in _arguments:
        if arg in _config["cli-modules"]:
            # Call this module instead
            func = _config["cli-modules"][arg]
            if len(func) > 1:
                response = futils.call([func[0], func[1]], _config, context)
                if response is not None:
                    print(str(response))
            elif len(func) == 1:
                print("Error: No configuration options for", arg,
                      "- requires two arguments in a list (the module, and function, to call)")
            return

    # Choose config file for imports
    if cf.is_x('import-from', config=_config, value='python'):
        # Import from config.py (might be configurable later)
        print("Importing from config.py.")
        global phtgenerator
        global phtparser
        global phtinitializer
        global phtworker
        # global phtworkflow
        from config import _GENERATOR   as phtgenerator
        from config import _PARSER      as phtparser
        from config import _INITIALIZER as phtinitializer
        from config import _WORKER     as phtworker
        # from config import _WORKFLOW as phtworkflow

    # Run the full templating engine

    print("Starting templater engine.")
    _config['data-dir'] = os.path.abspath(_config['data-dir'])
    _config['output-dir'] = os.path.abspath(_config['output-dir'])

    initializer = phtinitializer(_config, verbose)
    generator = phtgenerator(_config, verbose)
    parser = phtparser(_config, verbose)
    worker = phtworker(_config, verbose)

    # Get the topic from the initializer
    initializer.get_topic(context)

    # Get the template from the parser
    # Future - multiple templates, i.e. one could store the save data in JSON, etc
    template = run_async(
        parser.parse_from_file(
            cf.get_default('template', config=_config, default_value='./data/template.txt')
        ))

    # Now generate the output
    output = run_async(generator.generate(context=context, template=template))

    # Now finish up by giving the output to the worker
    worker.work(output, context)


if __name__ == "__main__":
    # If main() is used as a module, it should not be using sys.argv
    arguments = sys.argv
    # If main() is used as a module, it should not be worried about '--help'
    if '--help' in arguments:
        print("""Usage: python pht.py [--help]

                 Generates discussion files from templates, and more.

                 Optional arguments:
                 --help\t\tDisplay this help message and quit.
              """)
    if '--test' in arguments:
        print(arguments)
    else:
        main(arguments)
