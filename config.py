# Instead of using config.json, this can also be used to choose imports for the initializer/generator/parser/etc

from phtemplater.modules.default.pht_generator  import Generator    as _GENERATOR
from phtemplater.modules.default.pht_parser     import Parser       as _PARSER
from phtemplater.modules.pht_strawpoll_init     import Initializer  as _INITIALIZER
from phtemplater.modules.default.pht_worker     import Worker       as _WORKER